<?php
/*===========================>  MODEL  <===========================//

Parameters: table name.		

		---> FUNCTIONS <---				---> RETURN VALUES <---

	- insert	(cols, vals)		- new id of inserted row.
	- select	(predicate)			- query result in associative array.
	- update	(cols, vals, id)	- number of rows affected.
	- delete	(sql)				- number of rows affected.
	
	- getTable						- $this->table
	- getColumns					- $this->getColumns
	
	- setTable						- SETS table
	
	
Custom Functions: pass in 100% custom SQL and it will work.

	- insertCustom			- new id of inserted row.
	- selectCustom			- query result in associative array.
	- updateCustom			- number of rows affected.
	- deleteCustom			- number of rows affected.


NOTES: 'select' used to un-nest array by default.
				=> now takes a 2ND PARAMETER (any true value prevents nest)

//================================================================*/


class Model {	// Bring up and query a table by @param name.
	
	private $table;
	private $mysqli;		// DEDICATED mysqli, only one we have.
	private $columns;	// table columns for insert
		
	function __construct($table = ''){
		
		global $db_loc, $db_user, $db_pass, $db_name;
		
		$this->table = $table;	// 'table' passed in as param
		
		// Initialize mysqli.
		$this->mysqli = new mysqli($db_loc, $db_user, $db_pass, $db_name);
		if($this->mysqli->connect_errno) {
			die("DIED IN CONSTRUCTOR WITH THIS ERROR: %s\n".$this->mysqli->connect_error);
		}
		
		$res = $this->mysqli->query('SHOW COLUMNS FROM '.$this->table);
		while($row = $res->fetch_assoc()){
			$this->columns[] = $row['Field'];
		}
		
		return TRUE;
	}
	
	
/*=========================>  FUNCTIONS  <========================*/
	
	function insert( $cols = array(), $vals = array() ){
		
		if(empty($cols) or empty($vals)){
			die("NO COLUMNS OR VALUES SUPPLIED TO INSERT METHOD IN DATA");
		}
		
		// Query syntax
		$sql_cols = '';	// 	 NO quotes on columns.
		$sql_vals = '"';//		quotes on values.
		
		// Implode arrays
		if(is_array($cols) and is_array($vals)){
			$sql_cols .= implode( ', ' , $cols );
			$sql_vals .= implode( '", "' , $vals );
		}
		
		$sql_cols .= '';
		$sql_vals .= '"';
		
		$sql = "INSERT INTO `".$this->table."`(".$sql_cols." ) VALUES ( ".$sql_vals." );";
		// End query syntax
		
		
		if($this->mysqli->query($sql) === TRUE) { // return new id
			$id = $this->mysqli->insert_id;
			return $id;
			
		}else{	// Return and manage errors.
			
			$errors = mysqli_error_list($this->mysqli);
			if(sizeof($errors) == 1){
				return $errors[0];
			}else{
				return $errors;
			}
		}
	}	// End insert.
	
	function select($predicate = '', $prevent_nested = 0){	// $param predicate, i.e. where clause
										// Add in $param columns!
		
		$sql = "SELECT * FROM ".$this->table." ".$predicate;
		
		if($result = $this->mysqli->query($sql)){
			while ($row = $result->fetch_assoc()){
				$output[] = $row;
			}
		}
		
		if(sizeof($output == 1) and $prevent_nested){	// Prevent nested output.
			return $output[0];
			
		}elseif($output){		// Return standard output.
			return $output;
			
		}else{				// Return and manage errors.
			
			$errors = mysqli_error_list($this->mysqli);
			if(sizeof($errors) == 1){
				return $errors[0];
			}else{
				return $errors;
			}
		}
	}	// End select.
	
	function update($cols, $vals, $id){	// ok with $id like this?
		
		$sql = 'UPDATE '.$this->table.' SET ';
		
		if(is_array($cols) and is_array($vals)){
			$c = 0;
			foreach($cols as $col){
				$sql .= $col."='".$vals[$c]."', "; 
				$c++;
			}
			
		}elseif(!empty($cols) and !empty($vals)){
			
			$sql .= $cols . "='" .$vals."', ";
			
		}
		
		$sql = rtrim($sql, ', ');
		$sql .= ' WHERE id='.$id;
		
		if ($result = $this->mysqli->query($sql) === TRUE) { 
			return mysqli_affected_rows($this->mysqli);
			
		}else{	// Handle errors
			
			$errors = mysqli_error_list($this->mysqli);
			if(sizeof($errors) == 1){
				return $errors[0];
			}else{
				return $errors;
			}
		}
	}
	
	function delete($sql = ''){	// This one still gets sql.
			
		if ($result = $this->mysqli->query($sql) === TRUE) { 
			return mysqli_affected_rows($this->mysqli);
			
		}else{	// Handle errors
			
			$errors = mysqli_error_list($this->mysqli);
			if(sizeof($errors) == 1){
				return $errors[0];
			}else{
				return $errors;
			}
		}
	}
	
	function count($where = ''){	// Return an integer count
		
		// Build sql (where clause optional)
		$sql = 'SELECT COUNT(*) FROM '.$this->table;
		$sql .= (!empty($where)) ? ' '.$where : '';
		
		// Run the jewels
		if ($result = $this->mysqli->query($sql)) { 
			$row = $result->fetch_row();
			return intval($row[0]);
		}else{	// Handle errors
			$errors = mysqli_error_list($this->mysqli);
			if(sizeof($errors) == 1){
				return $errors[0];
			}else{
				return $errors;
			}
		}
	}

	
/*====================>   CUSTOM FUNCTIONS   <====================*/
	
	function insertCustom($sql = ''){
		if($this->mysqli->query($sql) === TRUE) { // return new id
			$id = $this->mysqli->insert_id;
			return $id;
		}else{	// Return and manage errors.
			$errors = mysqli_error_list($this->mysqli);
			if(sizeof($errors) == 1){
				return $errors[0];
			}else{
				return $errors;
			}
		}
	}	// End insert.
	
	function selectCustom($sql = '', $numeric_index = FALSE){
		
		if(!$numeric_index){	// Fetch normal assoc array
			if($result = $this->mysqli->query($sql)){
				while ($row = $result->fetch_assoc()){
					$output[] = $row;
				}
			}
		}else{		// Fetch numeric array
			if($result = $this->mysqli->query($sql)){
				while ($row = $result->fetch_array(MYSQLI_NUM)){
					$output[] = $row[0];
				}
			}
		}
		if(sizeof($output) == 1){	// Prevent nested output.
			return $output[0];
		}elseif($output){		// Return standard output.
			return $output;
		}else{				// Return and manage errors.
			$errors = mysqli_error_list($this->mysqli);
			if(sizeof($errors) == 1){
				return $errors[0];
			}else{
				return $errors;
			}
		}
	}	// End select.
	
	function updateCustom($sql = ''){
		if ($result = $this->mysqli->query($sql) === TRUE) { 
			return mysqli_affected_rows($this->mysqli);
		}else{	// Handle errors
			$errors = mysqli_error_list($this->mysqli);
			if(sizeof($errors) == 1){
				return $errors[0];
			}else{
				return $errors;
			}
		}
	}
	
	function deleteCustom($sql = ''){
		if ($result = $this->mysqli->query($sql) === TRUE) { 
			return mysqli_affected_rows($this->mysqli);		
		}else{	// Handle errors
			$errors = mysqli_error_list($this->mysqli);
			if(sizeof($errors) == 1){
				return $errors[0];
			}else{
				return $errors;
			}
		}
	}
	
	
	function getTable(){
		return $this->table;
	}
	
	function getColumns(){
		return $this->columns;
	}
	
	function setTable($table){
		$this->table = $table;
	}
}


/* BACKUP INSERT


function insert( $cols = array(), $vals = array() ){
		
	if(empty($cols) or empty($vals)){
		die("NO COLUMNS OR VALUES SUPPLIED TO INSERT METHOD IN DATA");
	}
	
	$sql_cols = '';	// 	 NO quotes on columns.
	$sql_vals = '"';//		quotes on values.
	
	// Implode arrays into query format
	if(is_array($cols) and is_array($vals)){
		$sql_cols .= implode( ', ' , $cols );
		$sql_vals .= implode( '", "' , $vals );
	}
	
	$sql_cols .= '';
	$sql_vals .= '"';
	
	$sql = "INSERT INTO `".$this->table."`(".$sql_cols." ) VALUES ( ".$sql_vals." );";
	
	if($this->mysqli->query($sql) === TRUE) { // return new id
		
		$id = $this->mysqli->insert_id;
		return $id;
		
	}else{	// Return and manage errors.
		
		$errors = mysqli_error_list($this->mysqli);
		
		if(sizeof($errors) == 1){
			return $errors[0];
		}else{
			return $errors;
		}
	}
}	// End insert.


// BACKUP UPDATE(even worse!)

function update($cols, $vals, $userID){
		
		$sql = 'UPDATE '.$this->table.' SET ';
		
		if(is_array($cols) and is_array($vals)){
			$c = 0;
			foreach($cols as $col){
				$sql .= $col."='".$vals[$c]."', "; 
				$c++;
			}
			
		}elseif(!empty($cols) and !empty($vals)){
			
			$sql .= $cols . "='" .$vals."', ";
			
		}
		
		$sql = rtrim($sql, ', ');
		$sql .= ' WHERE id='.$userID;
		
		if ($result = $this->mysqli->query($sql) === TRUE) { 
			return mysqli_affected_rows($this->mysqli);
			
		}else{	// Handle errors
			
			$errors = mysqli_error_list($this->mysqli);
			if(sizeof($errors) == 1){
				return $errors[0];
			}else{
				return $errors;
			}
		}
	}
	*/
