<?php 
/*==========>  GLOBAL CONFIG SETTINGS.  <==========*/

// ===> Meta Tags
$sitename = 'Ian Kendall';
$meta_auth = 'Ian Kendall';
$meta_desc = 'UX Architect';
$mega_keys = 'Ian Kendall Ian E. Kendall Web Developer Web Designer UI Designer UI Architect UI Developer UX Designer UX Architect UX Developer Architect';


// ===> DB Creds
// LOCAL
$db_loc =  '127.0.0.1';
$db_user = 'root';
$db_pass = 'root';
$db_name = 'test';

// PROD
// $db_loc = 'localhost';
// $db_user = 'gorillasaws';
// $db_pass = 'g0r1llas';
// $db_name = 'gorillas';