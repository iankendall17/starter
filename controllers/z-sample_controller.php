<?php
/*====================== ++ SETUP =====================*/
$table = 'test';
$tm = new Model($table);
$cols = $tm->getColumns();
$rows = $tm->count();

/*====================== // SETUP =====================*/
/*====================== ++ HEAD ======================*/
ob_start();	?>



<?php $OUTPUT['head'] = ob_get_contents();
ob_clean();
/*====================== // HEAD ======================*/
/*====================== ++ BODY ======================*/
ob_start();	?>

<h1>Oh, Hello.</h1>
<h3>You are modeling the '<?=$table;?>' table.</h3>
<p>Is has <?=$rows;?> rows.</p>
<p>These are the columns:</p>
<ul>
<?php foreach($cols as $col){		// Show table columns 	?>
	<li><?=$col;?></li>
<?php }	?>
</ul>

<?php	
$OUTPUT['body'] = ob_get_contents();
ob_clean();
/*====================== // BODY ======================*/
/*====================== ++ FOOT ======================*/
ob_start();
?>



<?php 
$OUTPUT['foot'] = ob_get_contents();
ob_clean();
/*====================== // FOOT ======================*/