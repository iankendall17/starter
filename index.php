<?php
/*======================>  INDEX.PHP  <=====================//
All frontend requests start here.


//==========================================================*/

// If your server's fucked, this'll tell you what to fix.
$LOST = FALSE;
session_start();

// Global frontend config & dependencies.
require_once('config.php');						// => Global config variables
require_once('classes/Model.php');		// => Database table model
require_once('help/help.php');				// => Helper functions

// URL => all routing done through this object.
$URL = array();
if (!empty($_GET['qs'])) {
	$URL = explode('/', $_GET['qs']);
}

// CONTROLLER => current file that runs the browser.
$CONTROLLER = 'home';
if (!empty($URL[0])) {
	$FILEPATH = 'controllers/'.$URL[0].'.php';
	if (is_file($FILEPATH)) {
		$CONTROLLER = $URL[0];
	} else {
		$CONTROLLER = '404';
	}
}

// FILEPATH => destination of controller file, included and sent
$FILEPATH = 'controllers/'.$CONTROLLER.'.php';
if($LOST){ die($FILEPATH);}
if (is_file($FILEPATH)) {
	include($FILEPATH);
}

$PAGETITLE = $sitename.' | '.ucfirst($CONTROLLER);
// Flush everything through site template & out.
include 'template.php';