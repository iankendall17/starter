<?php // TEMPLATE FILE. BEGINNING AND END TO ALL OF THE HTMLS.

?>
<!DOCTYPE html>
<!--[if lte IE 6]><html class="preIE7 preIE8 preIE9"><![endif]-->
<!--[if IE 7]><html class="preIE8 preIE9"><![endif]-->
<!--[if IE 8]><html class="preIE9"><![endif]-->
<!--[if gte IE 9]><!-->
<html>
<!--<![endif]-->
<head>
	<title><?=$PAGETITLE;?></title>
  
	<!-- Metas -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="author" content="<?=$meta_auth;?>">
	<meta name="description" content="<?=$meta_desc;?>">
	<meta name="keywords" content="<?=$meta_keys;?>">
	
	<!-- Styles -->
	<link rel="stylesheet" type="text/css" href="css/app.css">
	<link rel="icon" href="img/favicon.jpg">
	
	<!-- jQuery -->
	<script src="plugins/jquery-3.3.1.min.js"></script>
	
	<!-- Output controller head code -->
	<?=$OUTPUT['head'];?>
	
</head>
<body>
<?php 
/*===================== ++ SITE NAV =====================*/	
?>

            <!-- Write your nav here. -->
  
  
<?php 
/*===================== // SITE NAV =====================*/	
?>

<!-- Output controller body -->
<?=$OUTPUT['body'];?>
<!-- End controller body. -->

<?php 
/*===================== ++ SITE FOOTER =====================*/	
?>

            <!-- Write your footer here. -->

<?php 
/*===================== // SITE FOOTER =====================*/	
?>

<!-- Output controller foot -->
<?= $OUTPUT['foot']; ?>

<!-- Global js -->
<script src="js/app.js"></script>
</body>

</html>
